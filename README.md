Thread URL for which this repository is following: https://www.vg-resource.com/thread-34836.html

[Blender](https://www.blender.org/) is used for working with the main files (.blend).
The models found here are near-straight imports from the original files, using my rewritten homemade Blender Python script at <https://gitlab.com/Worldblender/io_scene_numdlb/blob/master/SSBUlt_NUMDLB.py>. All of them are animation-ready, meaning that animation files can be applied to them right away. The default settings currently assume that Blender Render is the current rendering engine, but EEVEE will be used for Blender 2.80 and later.

[Switch-Toolbox](https://github.com/KillzXGaming/Switch-Toolbox) is used to perform the texture exports found here and at <https://gitlab.com/Worldblender/smash-ultimate-textures>.

Current status of models (appear only if they have files here):
**Check the file <./model-status-table.html> for the status of models appearing in this repository.**
